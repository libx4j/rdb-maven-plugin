/* Copyright (c) 2016 lib4j
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * You should have received a copy of The MIT License (MIT) along with this
 * program. If not, see <http://opensource.org/licenses/MIT/>.
 */

package org.libx4j.maven.plugin.rdb;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Execute;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.lib4j.maven.mojo.GeneratorMojo;
import org.lib4j.maven.mojo.ResourceLabel;
import org.lib4j.net.URLs;
import org.lib4j.xml.ValidationException;
import org.libx4j.rdb.jsql.generator.Generator;

@Mojo(name="ddlx2jsql", defaultPhase=LifecyclePhase.GENERATE_SOURCES)
@Execute(goal="ddlx2jsql")
public final class Ddlx2JsqlMojo extends GeneratorMojo {
  @Parameter(property="schemas", required=true)
  private List<String> schemas;

  @Override
  @SuppressWarnings("unchecked")
  @ResourceLabel(label="schemas", nonEmpty=true)
  protected List<String>[] getResources() {
    return new List[] {schemas};
  }

  @Override
  public void execute(final Configuration configuration) throws MojoExecutionException, MojoFailureException {
    try {
      for (final URL resource : configuration.getResources(0))
        new Generator(resource).generate(URLs.getShortName(resource), configuration.getDestDir());
    }
    catch (final IOException | ValidationException e) {
      throw new MojoExecutionException(e.getMessage(), e);
    }
  }
}