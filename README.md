<img src="https://www.cohesionfirst.org/logo.png" align="right">

## rdb-maven-plugin<br>![mvn-plugin][mvn-plugin] <a href="https://www.cohesionfirst.org/"><img src="https://img.shields.io/badge/CohesionFirst%E2%84%A2--blue.svg"></a>
> Maven Plugin for [RDB][rdb] framework

### Introduction

The `rdb-maven-plugin` plugin is used to execute database-related generators, which are currently the [RDB][rdb] framework.

### Goals Overview

* [`rdb:ddlx`](#rdbddlx) generates .sql schema from .ddlx.
* [`rdb:sqlx`](#rdbsqlx) generates .xsd schema from .ddlx.
* [`rdb:jsql`](#rdbjsql) generates jSQL Entities from .ddlx.

### Usage

#### `rdb:ddl`

The `rdb:ddl` goal is bound to the `generate-resources` phase, and is used to generate DDL schema files from XML files conforming to the [DDLx Schema][ddlx-schema].

##### Example

```xml
<plugin>
  <groupId>org.libx4j.maven.plugin</groupId>
  <artifactId>rdb-maven-plugin</artifactId>
  <version>0.9.9-SNAPSHOT</version>
  <executions>
    <execution>
      <goals>
        <goal>ddlx</goal>
      </goals>
      <configuration>
        <vendor>PostgreSQL</vendor>
        <destDir>${project.build.directory}/generated-resources/rdb</destDir>
        <schemas>
          <schema>src/main/resources/resource.ddlx</schema>
        </schemas>
      </configuration>
    </execution>
  </executions>
</plugin>
```

#### Configuration Parameters

| Name              | Type    | Use      | Description                                                                   |
|:------------------|:--------|:---------|:------------------------------------------------------------------------------|
| `/vendor`         | String  | Required | Target vendor of generated DDL.                                               |
| `/destDir`        | String  | Required | Destination path of generated bindings.                                       |
| `/schemas`        | List    | Required | List of `schema` elements.                                                    |
| `/schemas/schema` | String  | Required | File path of XML Schema.                                                      |

#### `rdb:sqlx`

The `rdb:sqlx` goal is bound to the `generate-resources` phase, and is used to generate an XML Schema to allow one to create a validating SQLx file for static data.

##### Example

```xml
<plugin>
  <groupId>org.libx4j.maven.plugin</groupId>
  <artifactId>rdb-maven-plugin</artifactId>
  <version>0.9.9-SNAPSHOT</version>
  <executions>
    <execution>
      <goals>
        <goal>sqlx</goal>
      </goals>
      <configuration>
        <vendor>PostgreSQL</vendor>
        <destDir>${project.build.directory}/generated-resources/rdb</destDir>
        <schemas>
          <schema>src/main/resources/schema.ddlx</schema>
        </schemas>
      </configuration>
    </execution>
  </executions>
</plugin>
```

#### Configuration Parameters

| Name              | Type    | Use      | Description                                                                   |
|:------------------|:--------|:---------|:------------------------------------------------------------------------------|
| `/destDir`        | String  | Required | Destination path of generated bindings.                                       |
| `/schemas`        | List    | Required | List of `schema` elements.                                                    |
| `/schemas/schema` | String  | Required | File path of XML Schema.                                                      |

#### `rdb:jsql`

The `rdb:jsql` goal is bound to the `generate-sources` phase, and is used to generate jSQL Entities from XML files conforming to the [DDLx Schema][ddlx-schema].

##### Example

```xml
<plugin>
  <groupId>org.libx4j.maven.plugin</groupId>
  <artifactId>rdb-maven-plugin</artifactId>
  <version>0.9.9-SNAPSHOT</version>
  <executions>
    <execution>
      <goals>
        <goal>jsql</goal>
      </goals>
      <configuration>
        <vendor>PostgreSQL</vendor>
        <destDir>${project.build.directory}/generated-sources/rdb</destDir>
        <schemas>
          <schema>src/main/resources/schema.ddlx</schema>
        </schemas>
      </configuration>
    </execution>
  </executions>
</plugin>
```

#### Configuration Parameters

| Name              | Type    | Use      | Description                                                                   |
|:------------------|:--------|:---------|:------------------------------------------------------------------------------|
| `/destDir`        | String  | Required | Destination path of generated bindings.                                       |
| `/schemas`        | List    | Required | List of `resource` elements.                                                  |
| `/schemas/schema` | String  | Required | File path of XML Schema.                                                      |

### License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details.

[ddlx-schema]: https://github.com/libx4j/rdb/blob/master/ddlx/src/main/resources/ddlx.xsd
[mvn-plugin]: https://img.shields.io/badge/mvn-plugin-lightgrey.svg
[rdb]: https://github.com/libx4j/rdb